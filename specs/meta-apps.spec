%global d_bin                   %{_bindir}

Name:                           meta-apps
Version:                        1.0.0
Release:                        1%{?dist}
Summary:                        META-package for install apps
License:                        GPLv3

Source10:                       app.acme.sh
Source11:                       app.backup.sh
Source12:                       app.checksum.sh
Source13:                       app.domain.sh
Source14:                       app.storage.install.sh
Source15:                       app.usb.install.sh

Requires:                       bash zsh

%description
META-package for install apps.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0755 %{SOURCE10} \
  %{buildroot}%{d_bin}/app.acme.sh
%{__install} -Dp -m 0755 %{SOURCE11} \
  %{buildroot}%{d_bin}/app.backup.sh
%{__install} -Dp -m 0755 %{SOURCE12} \
  %{buildroot}%{d_bin}/app.checksum.sh
%{__install} -Dp -m 0755 %{SOURCE13} \
  %{buildroot}%{d_bin}/app.domain.sh
%{__install} -Dp -m 0755 %{SOURCE14} \
  %{buildroot}%{d_bin}/app.storage.install.sh
%{__install} -Dp -m 0755 %{SOURCE15} \
  %{buildroot}%{d_bin}/app.usb.install.sh


%files
%{d_bin}/app.acme.sh
%{d_bin}/app.backup.sh
%{d_bin}/app.checksum.sh
%{d_bin}/app.domain.sh
%{d_bin}/app.storage.install.sh
%{d_bin}/app.usb.install.sh


%changelog
* Sat Oct 19 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-1
- Initial build.
