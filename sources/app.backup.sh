#!/usr/bin/env bash

(( ${EUID} == 0 )) &&
  { echo >&2 "This script should not be run as root!"; exit 1; }
